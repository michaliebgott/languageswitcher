<?php

return [
	'frontend' => [
		'Codesly/Languageswitch/Languageswitcher' => [
			'target' => \Codesly\Languageswitch\Middleware\Languageswitcher::class,
			'before' => [
				'typo3/cms-frontend/base-redirect-resolver'
			]
		]
	]
];
