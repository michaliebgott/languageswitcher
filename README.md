# Languageswitcher Extension
TYPO3 Languageswitch based on Site Configuration

## Dependencies
- TYPO3 > 9.5

## Installation & Configuration
1. Configure Site Configuration
2. Simply install and activate the extension

## Author
**[Michael Liebgott](https://www.codesly.de)**
