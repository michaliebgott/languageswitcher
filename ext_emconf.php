<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Languageswitcher',
    'description' => 'TYPO3 Languageswitch based on Site Configuration',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'clearcacheonload' => 1,
    'author' => 'Michael Liebgott',
    'author_email' => 'm.liebgott@codesly.de',
    'author_company' => 'CODESLY',
    'constraints' => [
        'depends' => [
            'typo3' => '9.0.0-9.5.99'
        ]
    ],
    'autoload' => [
        'psr-4' => [
            'Codesly\\Languageswitch\\' => 'Classes'
        ],
    ],
];
