<?php

namespace Codesly\Languageswitch\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\RedirectResponse;

class Languageswitcher implements MiddlewareInterface {
    /**
     * @var array
     */
    public $request = [];

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $this->request = $request;
        $preferredLanguages = $this->getPreferredLanguages();
        $systemLanguages = $this->getSysLanguages();
        $language = null;

        foreach ($preferredLanguages as $preferredLanguage) {
            if (isset($systemLanguages[$preferredLanguage[0]])) {
                $language = $systemLanguages[$preferredLanguage[0]];
                break;
            }
        }

        if ($language === null) {
            $language = $languages[0];
        }

        if (strlen($this->request->getUri()->getPath()) == 1) {
            if (
                $language !== null 
                && $language->getBase()->getHost() == $this->request->getHeaders()['host'][0]
            ) {
                return new RedirectResponse($language->getBase(), 302);
            }
        } else {
            if (
                strpos($this->request->getUri()->getPath(), 'sitemap.xml')
                && substr_count($this->request->getUri()->getPath(), '/') < 2
                && $language->getBase()->getHost() == $this->request->getHeaders()['host'][0]
            ) {
                return new RedirectResponse($language->getBase()->getPath() . 'sitemap.xml', 302);
            }
        }
        
        return $handler->handle($this->request);
    }

    protected function getSysLanguages():array {
        $languagesArr = [];
        $languages = $this->request->getAttribute('site')->getLanguages();

        foreach ($languages as $language) {
            if (!empty($language->getTwoLetterIsoCode())) {
                $languagesArr[$language->getTwoLetterIsoCode()] = $language;
            }
        }

        return $languagesArr;
    }

    protected function getPreferredLanguages():array {
        $languagesArr = GeneralUtility::trimExplode(',', GeneralUtility::getIndpEnv('HTTP_ACCEPT_LANGUAGE'));
        $languagesArr = array_map(function($language) {
            $language = GeneralUtility::trimExplode(';q=', $language, 2);
            $language[0] = substr($language[0], 0, 2);
            
            if (count($language) === 1) {
                $language[1] = 1;
            } else {
                $language[1] = floatval($language[1]);
            }
            
            return $language;
        }, $languagesArr);
        usort($languagesArr, function($a, $b) {
            return $b[1] <=> $a[1];
        });

        return $languagesArr;
    }
}
